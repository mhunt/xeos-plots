# Plots from an Iridium Web Console (IWC) Report

### Usage:
To use this code, download a [report](https://xeos.passcal.nmt.edu/admin/reports.cgi)  from the Iridum Web Console (IWC) for the station of interest, selecting all fields. This will be the input CSV into the code.

In the 2nd cell of the Jupyter Notebooks file (xeos\_plots.ipynb), type in the following inputs:
- `csv_path`: Path to input CSV
- `save_path`: Path to save output figures in. Set to `None` if you don't want to save the figures.
- `title`: Title to add to all figures (e.g. station name). Set to `None` if you don't want a title.

Then, run all cells. A preview of the images will appear in the Jupyter Notebooks file, and they will also be saved at the indicated path.


### Example Graphs:
Here are sample graphs generated for TIME station LOUS between Jan 2020 to Jan 2021. The CSV input into the code is in the example\_plots folder.

Satellites:

![Satellites](/example_plots/satellites.png "Satellites")

Mass Positions:

![Mass Positions](/example_plots/mass_positions.png "Mass Positions")

Power:

![Power](/example_plots/power.png "Power")

Boots and Resyncs:

![Boots and Resyncs](/example_plots/boots_resyncs.png "Boots and Resyncs")

Time Error:

![Time Error](/example_plots/time_error.png "Time Error and VCO")

Messages:

![Messages](/example_plots/messages.png "Messages per Day")


### What if I want a graph that isn't one of the above?
The code in *utils.py* has a generalized function to plot any number of subplots. It should be fairly straightforward to modify the code to create graphs with other variables.


