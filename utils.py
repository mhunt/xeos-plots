'''
Utils functions for xeos_plots.
'''

from datetime import datetime as dtdt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import os

# set font size for all plots
plt.rcParams.update({'font.size': 12})


def create_dict(data, data_labels, colors, axis_label):
    '''
    Create a dictionary for easy input into plotting functions.
    Includes all data on the same subplot, with labelled x-axis.
    INPUTS:
        data : list of lists : list of all data
        data_labels : list of str : list of strings to label each data list with
        colors : list of str : list of colors to use to plot each data list
        axis_label : str : name of x-axis for subplot
    '''
    assert len(data) == len(data_labels) == len(colors), ('Number of data, labels, and colors should be the same.')
    
    data_dict = {'data': data,
                    'data_labels': data_labels,
                    'colors': colors,
                    'axis_label': axis_label}
    return data_dict


def plot_same_axis(time, title, y_dict):
    '''
    Plots multiple variables on the same y-axis. No subplots.
    INPUTS:
        time : list of str : list of date/times as strings
        y_dict : dict : dictionary of data, data_labels, colors, and axis_labels.
                Created using create_dict()
    RETURNS:
        Pyplot figure
    '''
    # initialize and set figure size (in inches)
    fig, ax = plt.subplots(figsize=(8, 6))
    
    # convert strings to datetime objects
    time_plot = [dtdt.strptime(i, '%Y-%m-%d %H:%M:%S') for i in time]
    
    lines = []
    y_data = y_dict['data']
    
    # plot each set of data
    for i, data in enumerate(y_data):
        l, = ax.plot(time_plot, data, color=y_dict['colors'][i],
                             label=y_dict['data_labels'][i],
                             marker='o', fillstyle='none', linestyle='None',
                             alpha=0.5, ms=3)
        # for making a legend
        lines.append(l)

    # set axis labels
    ax.set_ylabel(y_dict['axis_label'])
    plt.grid('on')

    # create legend
    legend = ax.legend(lines, [d.get_label() for d in lines],
                            bbox_to_anchor=(0, 1.02, 1, 1.02),
                            loc='lower left', mode='expand',
                            ncol = len(lines))
    
    # make sure legend markers are a reasonable size
    for lh in legend.legendHandles:
        lh._legmarker.set_markersize(5) 

    # set format x-tick marks for dates
    ax.xaxis.set_major_locator(mdates.MonthLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    fig.autofmt_xdate()
    
    # add title to entire plot
    fig.suptitle(title)
    fig.tight_layout()


def plot_n_subplots(time, title, **kwargs):
    '''
    Plots multiple variables on different subplots.
    INPUTS:
        time : list of str : list of date/times as strings
        **kwargs : dicts : dictionaries of data, data_labels, colors, and axis_labels.
                Created using create_dict().
                Number of dicts input = number of subplots on the final figure.
    RETURNS:
        Pyplot figure
    '''
    # number of subplots is length of kwargs
    n = len(kwargs)

    # initialize and set figure size (in inches)
    fig, ax = plt.subplots(n, sharex=True, figsize=(8,6))

    # convert strings to datetime objects
    time_plot = [dtdt.strptime(i, '%Y-%m-%d %H:%M:%S') for i in time]

    # plot each set of data on separate subplot
    lines = []
    for i, (key, y_dict) in enumerate(kwargs.items()):
        
        # plot data in same subplot for one dict
        for j, data in enumerate(y_dict['data']):
            label = y_dict['data_labels'][j]
            color = y_dict['colors'][j]
            l, = ax[i].plot_date(time_plot, data,
                                     xdate=True, label=label, color=color,
                                     marker='o', ms=3, fillstyle='none',
                                     linestyle='None')
            # for making a legend
            lines.append(l)

        # set the y-axis name for each dict
        ax[i].set_ylabel(y_dict['axis_label'])
        ax[i].grid(b=True)

    # set x-axis label and format tick marks
    ax[0].set_xlabel('Date (Y-m-d)')
    ax[0].xaxis.set_major_locator(mdates.MonthLocator())
    ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    fig.autofmt_xdate()

    # create the legend and move it above the subplots
    legend = ax[0].legend(lines, [i.get_label() for i in lines],
            bbox_to_anchor=(0, 1.02, 1, 1.02),
            loc='lower left', mode='expand', ncol=len(lines))
    for lh in legend.legendHandles:
        lh._legmarker.set_markersize(5)

    # smush subplots together and remove x ticks from upper plots
    fig.subplots_adjust(hspace=0)
    tick_labels = [a.get_xticklabels() for a in fig.axes[:-1]]
    plt.setp(tick_labels, visible=False)
    
    # add title to entire plot
    fig.suptitle(title)
    fig.tight_layout()


def save_fig(save_path, fig_name):
    '''
    Saves the pyplot figure instead of printing to screen.
    Facecolor and transparency are set for jupyter notebooks use.
    '''
    plt.savefig(os.path.join(save_path, fig_name), bbox_inches='tight',
                   facecolor='w', transparent=False)


